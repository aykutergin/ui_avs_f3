import Vue from 'vue';
import { LogModel } from './log_exports';

const that = Vue.prototype;

export class LogService {
    static configurationId = 7;

    static pushLog(pId, lvlId, msg, payload) {
        var date = new Date();
        var now = date.toLocaleString()
        var ms = date.getMilliseconds()
        var dateTime = now + "." + ms

        var logModel = new LogModel(pId, lvlId, msg)

        if(payload != undefined) {
            logModel.message = logModel.message+ " : "+ JSON.stringify(payload)
        }

        logModel.confId = this.configurationId
        logModel.date = dateTime
        var data = {
            method: "PushLog",
            model: logModel,
        }

        console.log("Log data : ", data)

        that.$pushLog(data);
    }
}