export class ScrPacket {
    header;
    body;
    messageData;
}

export class Header {
    messageId;
    messageFlow;
    client;
    method;
}

export class Body {
    screen;
    cmd;
    data;
    languageData;
}