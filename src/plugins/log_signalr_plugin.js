import * as signalR from "@microsoft/signalr";

const PORT = process.env.NODE_ENV === 'production' ? 51956 : 9000;

const LogSignalRPlugin = {
    install(Vue) {
        const connection = new signalR.HubConnectionBuilder()
            .withUrl(`http://localhost:${PORT}/LogHub`)
            .withAutomaticReconnect()
            .build();

        Vue.prototype.$logHub = connection;

        connection.start().then(function () {
            console.log(`Log Connection Start `);
            connection.invoke("Register", connection.connectionId, "HtmlClient").catch(function (err) {
                console.log("Log Invoke Failed");
                return console.error(err.toString());
            });
        }).catch(function (err) {
            console.log("Log Connection Failed");
            return console.error(err.toString());
        })

        connection.onreconnected(connectionId => {
            console.log(`Connection onreconnected new connectionId:`+connectionId);
            connection.invoke("Register", connection.connectionId, "HtmlClient").catch(function (err) {
            console.log("Log Invoke Failed");
            return console.error(err.toString());
            });
        });


        Vue.prototype.$pushLog = function (data) {

            this.sendJson = JSON.stringify(data);
            console.log("Log Send Data: " + this.sendJson);

            connection.invoke(data.method, this.sendJson).then(() => {
                console.log(data.method + " Log Send success");
            }).catch((error) => {
                console.log(data.method + " Log Send fail :" + error);
            });
        }
    }
}

export default LogSignalRPlugin;