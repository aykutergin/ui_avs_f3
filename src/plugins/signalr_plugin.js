import * as signalR from "@microsoft/signalr";
import { GlobalKeys } from "../model/global_keys";
import { MessageFlow } from './model/message_flow';
import { Header, ScrPacket } from "./model/scr_packet";

const PORT = process.env.NODE_ENV === 'production' ? 51956 : 9000;

const SignalRPlugin = {
    install(Vue) {
        const connection = new signalR.HubConnectionBuilder()
            .withUrl(`http://localhost:${PORT}/eventhub`)
            .withAutomaticReconnect()
            .build();

        Vue.prototype.$hub = connection;

        connection.start().then(function () {
            console.log(`Connection Start `);
            connection.invoke("Register", connection.connectionId, "HtmlClient").catch(function (err) {
                console.log("Invoke Failed");
                return console.error(err.toString());
            });
        }).catch(function (err) {
            console.log("Connection Failed");
            return console.error(err.toString());
        })

        connection.onreconnected(connectionId => {
            console.log(`Connection onreconnected new connectionId:`+connectionId);
            connection.invoke("Register", connection.connectionId, "HtmlClient").catch(function (err) {
            console.log("Invoke Failed");
            return console.error(err.toString());
            });
        });


        Vue.prototype.$invokeMethod = function (pbody) {
            var sendData = new ScrPacket();
            sendData.body = pbody;
            sendData.body.messageFlow = MessageFlow.HtmlToApp;
            sendData.header = new Header();
            sendData.header.client = GlobalKeys.clientKey;
            sendData.header.messageId = 1;
            sendData.header.messageFlow = MessageFlow.HtmlToApp;
            sendData.header.method = "ScrMessage";

            this.sendJson = JSON.stringify(sendData);
            console.log("Send Data: " + this.sendJson);

            connection.invoke(sendData.header.method, this.sendJson).then(() => {
                console.log(sendData.header.method + " Send success");
            }).catch((error) => {
                console.log(sendData.header.method + " Send fail :" + error);
            });
        }
    }
}

export default SignalRPlugin;



