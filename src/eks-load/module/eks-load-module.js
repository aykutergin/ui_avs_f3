
import { EksLoadMutations } from "../../enums/mutations";
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = "eksLoadModule";

export const EksLoadActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    componentKey: 0,
    infoMessage: "",
}
const mutations = {
    [EksLoadMutations.SET_DATA](state, payload) {
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Set Data Mutation`, { payload: payload, state: state })
    },
    [EksLoadMutations.CHANGE_DATA](state, payload) {
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Change Data Mutation`, { payload: payload, state: state })
    },
    [EksLoadMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload
    },
    [EksLoadMutations.SET_BUTTON_LABELS](state) {
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    },
}
const actions = {
    loadData({ commit }, payload) {
        console.log("EKS Load Data : ", payload)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Load Data`, payload)
        if (payload != null) {
            commit(EksLoadMutations.SET_DATA, payload.data)
            commit(EksLoadMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(EksLoadMutations.SET_BUTTON_LABELS)
        }
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}