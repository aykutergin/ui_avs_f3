import Vue from "vue";
import { CommonCommand, DebtQueryCommand } from "../../enums/command";
import { DebtInquiryMutations } from "../../enums/mutations";
import { ScreenEnum } from '../../enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = "inquiryModule";

export const DebtQueryActionTypes = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CHANGE_QPARAM_VALUE: `${moduleName}/changeQueryParamValue`,
    SELECTED_QTYPE: `${moduleName}/selectedQtype`,
    SELECTED_QPARAM: `${moduleName}/selectedQparam`,
    SELECTED_QPARAM_VALUE_CLEAR: `${moduleName}/clearQparamValue`,
}

const state = {
    title: "",
    langDictionary: null,
    queryTypes: [],
    selectedQueryType: null,
    queryParams: [],
    selectedQueryParam: null,
    showKeyboard: false,
    showNumberpad: false,
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
}

const mutations = {
    // Ekranın ilk yüklenmesinde çalışır. Listeler doldurulur.
    [DebtInquiryMutations.SET_QUERY_TYPES](state, payload) {
        state.queryTypes = []
        payload.qtypes.forEach(qType => {
            if (qType.flagVisible)
                state.queryTypes.push(qType)
        })

        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Data Mutation QueryTypes`, state.queryTypes)

        state.selectedQueryType = payload.qtypes.find(
            (qtype) => qtype.selected == true
        );

        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Data Mutation Selected Query`, state.selectedQueryType)
    },
    // Tüm inquiryTypes ların selected özelliğini false yapar ve içerisinde paramsların değerlerini sıfırlar.
    [DebtInquiryMutations.SET_QTYPES_SELECTED_DEFAULT](state) {
        state.queryTypes = state.queryTypes.map((qtype) => {
            qtype.selected = false;
            qtype.qparams.map((qparam) => qparam.qParamValue = null);
            return qtype;
        });
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Selected properties are removed and their values are reset`)
    },
    // Seçili qparam ın value değerini günceller.
    [DebtInquiryMutations.SET_QUERY_PARAM_VALUE](state, payload) {
        if (state.selectedQueryParam.qParamValue == null)
            state.selectedQueryParam.qParamValue = "";
        console.log('param değişiyor : ', payload)

        var currentLength = state.selectedQueryParam.qParamValue.length
        var limit = state.selectedQueryParam.paramSetting.length

        if (currentLength == limit) return;

        if (payload.flagNumeric) {

            state.selectedQueryParam.qParamValue = payload.value.toString();
        }
        else {
            state.selectedQueryParam.qParamValue += payload.value.toString();
        }
    },
    // Seçili inquiryType değerini günceller
    [DebtInquiryMutations.SET_SELECTED_QTYPE](state, payload) {
        payload.selected = true
        state.selectedQueryType = payload
        var index = state.queryTypes.findIndex((obj => obj.qTypeId == payload.qTypeId));
        state.queryTypes[index] = payload;
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Selected QueryType`, state.selectedQueryType)
    },
    // Seçili inquiryType üzerinden qparam listesini ve seçili qparamı ayarlar.
    // Numberpad ve Keyboardpad gösterim flag larını ayarlar.
    [DebtInquiryMutations.SET_QPARAMS_AND_SELECTED_PARAM](state) {
        state.queryParams = state.selectedQueryType.qparams;
        state.selectedQueryParam = state.queryParams[0]
        state.showKeyboard = state.selectedQueryParam.flagAlphaNumeric
        state.showNumberpad = !state.showKeyboard
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Sets Numberpad and Keyboardpad display flags`, { showKeyboard: state.showKeyboard, showNumberpad: state.showNumberpad })
    },
    // Seçili qparam ı günceller.
    [DebtInquiryMutations.SET_SELECTED_QPARAM](state, payload) {
        state.selectedQueryParam = payload;
        state.showKeyboard = state.selectedQueryParam.flagAlphaNumeric
        state.showNumberpad = !state.showKeyboard
        console.log("selectedQueryParam : ", state.selectedQueryParam)
        console.log("state.showKeyboard : ", state.showKeyboard)
        console.log("state.showNumberpad : ", state.showNumberpad)
    },
    // Silme butonuna basılınca qparam value'dan birer birer silme işlemini gerçekleştirir.
    [DebtInquiryMutations.SET_QPARAM_VALUE_CLEAR](state, payload) {
        console.log("qparam clear numberpad")
        if (payload != undefined && payload) {
            state.selectedQueryParam.qParamValue = null
        } else {
            if (state.selectedQueryParam != null) {
                console.log("selectedQueryParam : ", state.selectedQueryParam)
                if (state.selectedQueryParam.qParamValue.length > 0)
                    state.selectedQueryParam.qParamValue = state.selectedQueryParam.qParamValue.slice(0, -1);
            }
        }

    },


    [DebtInquiryMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload;
    },
    [DebtInquiryMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["debtQueryModule.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("inquiryModule loadData")
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Load Data Action`, payload)
        if (payload != null) {
            commit(DebtInquiryMutations.SET_QUERY_TYPES, payload.data)
            commit(DebtInquiryMutations.SET_QPARAMS_AND_SELECTED_PARAM)
            commit(DebtInquiryMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(DebtInquiryMutations.SET_BUTTON_LABELS)
        }
    },
    changeQueryParamValue({ commit }, data) {
        commit(DebtInquiryMutations.SET_QUERY_PARAM_VALUE, data)
    },
    selectedQtype({ commit }, data) {
        commit(DebtInquiryMutations.SET_QTYPES_SELECTED_DEFAULT)
        commit(DebtInquiryMutations.SET_SELECTED_QTYPE, data)
        commit(DebtInquiryMutations.SET_QPARAMS_AND_SELECTED_PARAM)
    },
    selectedQparam({ commit }, data) {
        commit(DebtInquiryMutations.SET_SELECTED_QPARAM, data)
    },
    clearQparamValue({ commit }, data) {
        commit(DebtInquiryMutations.SET_QPARAM_VALUE_CLEAR, data)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.DebtQuery;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.DebtQuery;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ commit }, selectedQueryType) {
        var selectedQueryTypeModel = {};
        selectedQueryTypeModel.qtype = selectedQueryType

        console.log(selectedQueryTypeModel);
        var body = {};
        body.cmd = DebtQueryCommand.DebtQueryInformation;
        body.screen = ScreenEnum.DebtQuery;
        body.data = JSON.stringify(selectedQueryTypeModel);
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] NEXT`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}