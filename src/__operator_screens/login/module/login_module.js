import Vue from "vue"
import { ScreenEnum } from '../../../enums/screen_enum'
import { CommonCommand } from "../../../enums/command"
import { LogService, LogPriorityEnum, LogLevelEnum } from '../../../plugins/log_exports'

const moduleName = "loginModule";

export const LoginActionTypes = {
    LOGIN: `${moduleName}/login`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
}
const mutations = {
}
const actions = {
    // eslint-disable-next-line no-unused-vars
    loadData({ commit }, payload) {
    },
    // eslint-disable-next-line no-unused-vars
    login({commit}, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Operator Login] Login Action`, payload)
        var loginData = {
            userTerm: payload.userTerm,
            password: payload.password,
        }

        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashAccepting;
        body.data = JSON.stringify(loginData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Operator Login] Login Invoke`, loginData)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.DebtQuery;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}