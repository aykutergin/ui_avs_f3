import Vue from "vue";
import { CommonCommand, UnitCommand } from "../../enums/command";
import { SelectionMutations } from "../../enums/mutations.js";
import { ScreenEnum } from '../../enums/screen_enum';

const moduleName = "processModule";

export const SelectionActionTypes = {
    SELECT: `${moduleName}/select`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    title: "",
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
    svcs: [],
    langDictionary: null,
}

const mutations = {
    [SelectionMutations.SET_TITLE](state, payload) {
        state.title = payload
    },
    [SelectionMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload
    },
    [SelectionMutations.SET_SVCS](state, payload) {
        state.svcs = [];
        payload.forEach(element => {
            state.svcs.push(element)
        });
    },
    [SelectionMutations.SET_BUTTON_LABELS](state) {
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    },
}

const actions = {
    loadData({ commit }, payload) {
        if (payload != null) {
            commit(SelectionMutations.SET_SVCS, payload.data.svcs)
            commit(SelectionMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            var title = payload.langDictionary["selectionModul.header"]
            commit(SelectionMutations.SET_TITLE, title)
            commit(SelectionMutations.SET_BUTTON_LABELS)
        }
    },
    changeData({commit}, payload){
        if (payload != null) {
            commit(SelectionMutations.SET_SVCS, payload.data.svcs)
        }
    },
    back() {
        console.log("back tıklandı");
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.Unit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.Unit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    select({ commit }, selectedProcService) {
        console.log("selected svcId=" + selectedProcService.svcId);
        var inSelectedUnitData = {};
        inSelectedUnitData.svcId = selectedProcService.svcId;

        var body = {};
        body.cmd = UnitCommand.UnitSelected;
        body.screen = ScreenEnum.Unit;
        body.data = JSON.stringify(inSelectedUnitData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}