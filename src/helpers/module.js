import { ScreenEnum } from "../enums/screen_enum";

export class Module {
    static setModuleLoad(screen) {
        switch (screen) {
            case ScreenEnum.Initialize:
                return "initializeModule/loadData";
            case ScreenEnum.Init:
                return "introModule/loadData";
            case ScreenEnum.Language:
                return "langModule/loadData";
            case ScreenEnum.Unit:
                return "selectionModule/loadData";
            case ScreenEnum.DebtQuery:
                return "inquiryModule/loadData";
            case ScreenEnum.DebtSelection:
                return "debtSelectionModule/loadData";
            case ScreenEnum.CashType:
                return "cashTypeModule/loadData";
            case ScreenEnum.CashAccepting:
                return "cashAcceptingModule/loadData";
            case ScreenEnum.CreditCard:
                return "creditCardModule/loadData";
            case ScreenEnum.CardReader:
                return "cardReaderModule/loadData";
            case ScreenEnum.CalculateCredit:
                return "calculateCreditModule/loadData";
            case ScreenEnum.Print:
                return "printModule/loadData";
            case ScreenEnum.Payback:
                return "paybackModule/loadData";
            case ScreenEnum.EksLoad:
                return "eksLoadModule/loadData";
            default:
                return "";
        }
    }

    static setModuleChange(screen) {
        switch (screen) {
            case ScreenEnum.Initialize:
                return "initializeModule/changeData";
            case ScreenEnum.Init:
                return "introModule/changeData";
            case ScreenEnum.Language:
                return "langModule/changeData";
            case ScreenEnum.Unit:
                return "selectionModule/changeData";
            case ScreenEnum.DebtQuery:
                return "inquiryModule/changeData";
            case ScreenEnum.DebtSelection:
                return "debtSelectionModule/changeData";
            case ScreenEnum.CashType:
                return "cashTypeModule/changeData";
            case ScreenEnum.CashAccepting:
                return "cashAcceptingModule/changeData";
            case ScreenEnum.CreditCard:
                return "creditCardModule/changeData";
            case ScreenEnum.CardReader:
                return "cardReaderModule/changeData";
            case ScreenEnum.CalculateCredit:
                return "calculateCreditModule/changeData";
            case ScreenEnum.Print:
                return "printModule/changeData";
            case ScreenEnum.Payback:
                return "paybackModule/changeData";
            case ScreenEnum.EksLoad:
                return "eksLoadModule/changeData";
            default:
                return "";
        }
    }
}