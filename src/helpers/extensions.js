Object.defineProperty(String.prototype, "checkLength", {
    value: function checkLength(length) {
        if(this.length > length) {
            var diff = length - this.length
            return this.slice(0, diff)+'...'
        } else return this
    },
    writable: true,
    configurable: true
});