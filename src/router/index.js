import Vue from 'vue';
import VueRouter from 'vue-router';
import CalculateCredit from '../calculate-credit/view/CalculateCredit.vue';
import CardReader from '../card-reader/view/CardReader.vue';
import CashAccepting from '../cash-accepting/view/CashAccepting.vue';
import CashTypeSelection from '../cash-type-selection/view/CashTypeSelection.vue';
import CreditCard from '../credit-card/view/CreditCard.vue';
import DebtList from '../debt-list/view/DebtList.vue';
import DebtInquiry from '../debt_inquiry/view/DebtInquiry.vue';
import EksLoad from '../eks-load/view/EksLoad.vue';
import Initialize from '../initialize/view/Initialize.vue';
import Introduction from '../introduction/view/Introduction.vue';
import Language from '../language/view/Language.vue';
import Payback from '../payback/view/Payback.vue';
import Print from '../print/view/Print.vue';
import ProcessService from '../process-service/view/ProcessService.vue';
import Home from '../views/Home.vue';
import Login from '../__operator_screens/login/view/Login.vue';
import { RoutePaths } from './route-paths';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/initialize',
    name: RoutePaths.Initialize,
    component: Initialize
  },
  {
    path: '/intro',
    name: RoutePaths.Introduction,
    component: Introduction
  },
  {
    path: '/lang',
    name: RoutePaths.Language,
    component: Language
  },
  {
    path: '/proc-service',
    name: RoutePaths.ProcService,
    component: ProcessService
  },
  {
    path: '/inquiry',
    name: RoutePaths.DebtInquiry,
    component: DebtInquiry
  },
  {
    path: '/debt-selection',
    name: 'debt-selection',
    component: DebtList
  },
  {
    path: '/cash-type',
    name: 'cash-type',
    component: CashTypeSelection
  },
  {
    path: '/cash-accepting',
    name: RoutePaths.CashAccepting,
    component: CashAccepting
  },
  {
    path: '/payback',
    name: RoutePaths.Payback,
    component: Payback
  },
  {
    path: '/credit-card',
    name: RoutePaths.CreditCard,
    component: CreditCard
  },
  {
    path: '/print',
    name: RoutePaths.Print,
    component: Print
  },
  {
    path: '/card-reader',
    name: RoutePaths.CardReader,
    component: CardReader
  },
  {
    path: '/calculate-credit',
    name: RoutePaths.CalculateCredit,
    component: CalculateCredit
  },
  {
    path: '/eks-load',
    name: RoutePaths.EksLoad,
    component: EksLoad
  },
  {
    path: '/login',
    name: RoutePaths.LOGIN,
    component: Login
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
