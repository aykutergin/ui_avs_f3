export const CashAcceptingMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    SET_LANG_DICTIONARY : 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    SET_CASH_MODELS: 'setCashModels',
    REFRESH: 'refresh',
}

export const InitializeMutations = {
    SET_MESSAGES : "setMessages",
}

export const IntroMutations = {
    SET_VIDEO : 'setVideo',
    SET_SLIDER : 'setSlider',
    SET_TYPE : 'setType'
}

export const LangManagementMutations = {
    SET_DICTIONARY : "setDictionary",
    GET_VALUE : 'getValue',
    SET_VALUE : 'setValue',
}

export const LangMutations = {
    SET_LANGUAGES : 'setLanguages',
    SET_BUTTON_LABELS : 'setButtonLabels',
    SET_LANG_DICTIONARY : 'setLangDictionary',
}

export const MessageMutations = {
    ADD_MESSAGE: 'addMessage',
    DELETE_MESSAGE: 'deleteMessage',
    CLEAR_MESSAGES: 'clearMessages',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
}

export const SelectionMutations = {
    SET_TITLE: 'setTitle',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_SVCS : 'setSvcs',
    SET_BUTTON_LABELS : 'setButtonLabels',
}

export const DebtInquiryMutations = {
    SET_QUERY_TYPES: 'setQueryTypes',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    SET_QUERY_PARAM_VALUE: 'setQueryParamValue',
    SET_QTYPES_SELECTED_DEFAULT: 'setQtypesSelectedDefault',
    SET_SELECTED_QTYPE: 'setSelectedQtype',
    SET_QPARAMS_AND_SELECTED_PARAM: 'setQparamsAndSelectedParam',
    SET_QPARAM_VALUE_CLEAR: 'setQparamValueClear',
    SET_SELECTED_QPARAM: 'setSelectedQparam',
}

export const CalculateCreditMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    CHANGE_AMOUNT: 'changeAmount',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    CLICKED_CANCEL: 'clickedCancel',
}

export const CreditCardMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    SET_TOTAL_BILL: 'setTotalBill',
    SET_CUSTOMER: 'setCustomer',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    CLICKED_CANCEL: 'clickedCancel',
}

export const CardReaderMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    SET_TOTAL_BILL: 'setTotalBill',
    SET_CUSTOMER: 'setCustomer',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    CLICKED_CANCEL: 'clickedCancel',
}

export const EksLoadMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
}

export const CashTypeMutations = {
    SET_PAY_TYPES : 'setPayTypes',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
}

export const PaybackMutations = {
    SET_MESSAGE: "setMessage",
    SET_TIMEOUT: "setTimeout",
    REFRESH: "refresh",
}

export const PrintMutations = {
    SET_DATA : 'setData',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
}

export const DebtSelectionMutations = {
    SET_DEBTS: 'setDebts',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    SET_TOTAL_PAYMENT: 'setTotalPayment',
    SET_TOTAL_BILL_COUNT: 'setTotalBillCount',
    SET_SELECTED_PROC: 'setSelectedProc',
    SET_SELECTED_BILL_GROUP: 'setSelectedBillGroup',
    SET_SELECTED_BILL: 'setSelectedBill',
    CHANGE_SELECTED_BILLS: 'changeSelectedBills',
    CHANGE_SELECTED_ALL: 'changeSelectedAll',
}