export const KeyboardKey = {
    DONE: '9091',
    SPACE: '9092',
    BACKSPACE: '9093',
    CTRL: '9094',
    SHIFT: '9095',
    ENTER: '9096',
    EMPTY: '9097',
    SPECIAL: '9098',
    ABC: '9099',
}