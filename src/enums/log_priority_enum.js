export const LogPriorityEnum = {
    Fatal: 0,
    Major: 1,
    Minor: 2,
    Warn: 3,
    Info: 4,
}