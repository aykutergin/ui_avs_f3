export const InitializeCommand = {
    InitializeLoad: 1000,
    Change: 1002,
    Completed: 1003,
}

export const InitCommand = {
    InitLoad: 2001,
    InitStartTransaction: 2002,
}

export const LanguageCommand = {
    LanguageLoad: 3001,
    LanguageSelected: 3002,
}

export const SelectionCommand = {
    SelectionLoad: 1,
    SelectionOpSelected: 2,
}

export const CommonCommand = {
    Timeout: 100,
    Back: 101,
    Cancel: 102,
}

export const UnitCommand = {
    UnitLoad: 4001,
    Changed: 4002,
    UnitSelected: 4003,
}

export const DebtQueryCommand = {
    DebtQueryLoad: 5001,
    DebtQueryInformation: 5002
}
export const DebtSelectionCommand = {
    Load : 6001,
    Selected: 6002,
    Changed: 6003,
    Completed: 6004,
}
export const CashTypeCommand = {
    CashTypeLoad: 7001,
    CashTypeSelected: 7002
}
export const CashAcceptingCommand = {
    CashAcceptingLoad: 8001,
    CashAcceptingUpdate: 8002,
    CashAcceptingComplete: 8003,
    ChangeReceived: 8004,
}
export const CreditCardCommand = {
    CreditCardLoad: 9001,
    CreditCardChange: 9002,
}
export const VirtualPosCommand = {
    VirtualPosLoad: 1,
}
export const CreditCardPinCommand = {
    CreditCardPinLoad: 1,
    CreditCardPinNum: 2
}
export const PaybackCommand = {
    PaybackLoad: 10001,
    PaybackSuccess: 10002,
    Timeout: 10003,
}
export const PrintCommand = {
    PrintLoad: 20001,
    Timeout: 20002,
}
export const MessageCommand = {
    Show: 200,
    Hide: 201,
    Responsed: 202,
}

export const CardReaderCommand = {
    CardReaderLoad: 30001,
}
export const CardInfoCommand = {
    CardInfoLoad: 1,
}
export const CalculateCreditCommand = {
    CalculateCreditLoad: 40001,
    CalculateCredit: 40002
}
export const CustomerWebCommand = {
    CustomerWebLoad: 1,
}
export const ActiveAvsCommand = {
    ActiveAvsLoad: 1,
}

export const EksLoadCommand = {
    Load: 50001,
    Change: 50002,
}