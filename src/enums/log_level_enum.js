export const LogLevelEnum = {
    L1: 1,
    L2: 2,
    L3: 3,
    L4: 4,
}