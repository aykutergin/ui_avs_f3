import Vue from "vue"
import { CommonCommand, CreditCardCommand } from "../../enums/command"
import { CreditCardMutations } from "../../enums/mutations"
import { ScreenEnum } from '../../enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports'

const moduleName = "creditCardModule";

export const CreditCardActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    componentKey: 0,
    title: "",
    totalBill: 0,
    customer: null,
    flagShowAnimation: true,
    infoMessage: "Kartınızı takınız",
    animationCode: 2,
    lblTotalBill: "",
    nextLabel: "",
    cancelLabel: "",
    prevLabel: "",
    clickedCancel: false,

    hideBackButton: false,
    hideCancelButton: false,
}
const mutations = {
    [CreditCardMutations.SET_DATA](state, payload) {
        state.clickedCancel = false
        state.hideBackButton = true
        state.hideCancelButton = false
        state.flagShowAnimation = true
        state.animationCode = payload.animationCode
        state.infoMessage = payload.infoMessage
        state.customer = payload.customer
        state.totalBill = payload.totalBill
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Set Data Mutation`, { payload: payload, state: state })
        console.log("data aktarımı tamamlandı : ", state)
    },
    [CreditCardMutations.CHANGE_DATA](state, payload) {
        state.hideBackButton = true
        state.hideCancelButton = true
        state.flagShowAnimation = payload.flagShow
        state.animationCode = payload.animationCode
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Change Data Mutation`, { payload: payload, state: state })
    },
    [CreditCardMutations.SET_LANG_DICTIONARY](state, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Set Lang Dictionary Mutation`, payload)
        state.langDictionary = payload
    },
    [CreditCardMutations.CLICKED_CANCEL](state, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Clicked Cancel Button`, payload)
        state.clickedCancel = payload
    },
    [CreditCardMutations.SET_BUTTON_LABELS](state) {
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
        state.lblTotalBill = state.langDictionary["ccModul.lblTotalBill"]
    },
}
const actions = {
    loadData({ commit }, payload) {
        console.log("Credit Card Load Data : ", payload)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Load Data`, payload)
        if (payload != null) {
            commit(CreditCardMutations.SET_DATA, payload.data)
            commit(CreditCardMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(CreditCardMutations.SET_BUTTON_LABELS)
        }
    },
    changeData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Change Data`, payload)
        if (payload != null) {
            commit(CreditCardMutations.CHANGE_DATA, payload.data);
        }
    },
    changeReceived() {
        var body = { };
        body.cmd = CreditCardCommand.CreditCardLoad;
        body.screen = ScreenEnum.CreditCard;
        body.data = "";
        console.log("Change Received: ", body);
        Vue.prototype.$invokeMethod(body);
    },
    back() {
        var body = { };
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CreditCard;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel({state, commit}) {
        if (!state.clickedCancel) {
            commit(CreditCardMutations.CLICKED_CANCEL, true);
            var body = {};
            body.cmd = CommonCommand.Cancel;
            body.screen = ScreenEnum.CreditCard;
            body.data = "";
            console.log(body);
            Vue.prototype.$invokeMethod(body);
        }

    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}