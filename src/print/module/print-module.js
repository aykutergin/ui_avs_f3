import Vue from "vue";
import { PrintCommand } from "../../enums/command";
import { PrintMutations } from "../../enums/mutations";
import { ScreenEnum } from '../../enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const state = {
    title: "",
    message: "Makbuzunuzu almayı unutmayın",
    timeout: 0,
}

const mutations = {
    [PrintMutations.SET_DATA](state, payload) {
        console.log("print setData : ", payload)
        state.message = payload.printBuffer
        state.timeout = payload.timeout
        console.log("message : ", state.message)
        console.log("timeout : ", state.timeout)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] SET DATA MUTATION`, { payload: payload, state: state })
    },
    [PrintMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload;
    },
    [PrintMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["printModul.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    }
}

const actions = {
    loadData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] LOAD DATA ACTION`, payload)
        console.log("print load data : ", payload)
        if (payload != null) {
            commit(PrintMutations.SET_DATA, payload.data)
            commit(PrintMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(PrintMutations.SET_BUTTON_LABELS)
        }
    },
    timeout() {
        var body = {};
        body.cmd = PrintCommand.Timeout;
        body.screen = ScreenEnum.Print;
        body.data = "";
        console.log("Print timeout ", body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] TIMEOUT`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}