import Vue from "vue"
import { CommonCommand } from "../../enums/command"
import { CardReaderMutations } from "../../enums/mutations"
import { ScreenEnum } from '../../enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports'

const moduleName = "cardReaderModule";

export const CardReaderActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    componentKey: 0,
    title: "",
    flagShowAnimation: true,
    infoMessage: "",
    animationCode: 0,
    arrowDirection: 0,
    clickableCancel: false,
    headerVisible: false,
    hideBackButton: false,
    hideCancelButton: false,
}
const mutations = {
    [CardReaderMutations.SET_DATA](state, payload) {
        state.clickableCancel = true
        state.hideBackButton = true
        state.hideCancelButton = false
        state.flagShowAnimation = true
        state.animationCode = payload.animationCode
        state.infoMessage = payload.infoMessage
        state.arrowDirection = payload.arrowDirection
        state.headerVisible = payload.headerVisible
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Set Data Mutation`, { payload: payload, state: state })
        console.log("data aktarımı tamamlandı : ", state)
    },
    [CardReaderMutations.CHANGE_DATA](state, payload) {
        state.hideBackButton = true
        state.hideCancelButton = true
        state.flagShowAnimation = payload.flagShow
        state.animationCode = payload.animationCode
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Change Data Mutation`, { payload: payload, state: state })
    },
    [CardReaderMutations.SET_LANG_DICTIONARY](state, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Set Lang Dictionary Mutation`, payload)
        state.langDictionary = payload
    },
    [CardReaderMutations.CLICKED_CANCEL](state, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Clicked Cancel Button`, payload)
        state.clickableCancel = payload
    },
    [CardReaderMutations.SET_BUTTON_LABELS](state) {
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
        state.title = state.langDictionary["cardReaderModul.header"]
        state.componentKey++
        console.log("title : ", state.title)
        console.log("state.cancelLabel : ", state.cancelLabel)
    },
}
const actions = {
    loadData({ commit }, payload) {
        console.log("Card Reader Load Data : ", payload)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Load Data`, payload)
        if (payload != null) {
            commit(CardReaderMutations.SET_DATA, payload.data)
            commit(CardReaderMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(CardReaderMutations.SET_BUTTON_LABELS)
        }
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.CardReader;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] CANCEL İŞLENDİ`, body)
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}