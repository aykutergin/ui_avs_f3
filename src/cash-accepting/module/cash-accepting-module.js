import Vue from "vue"
import { CashAcceptingCommand, CommonCommand } from "../../enums/command"
import { CashAcceptingMutations } from "../../enums/mutations.js"
import { ScreenEnum } from '../../enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports'

const moduleName = "cashAcceptingModule";

export const CashAcceptingActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CHANGE_RECEIVED: `${moduleName}/changeReceived`,
}

const state = {
    componentKey: 0,
    title: "",
    infoMessage: "",
    totalBill: 0.0,
    accepted: 0.0,
    customer: null,
    cashModels: [],
    flagDetail: false,
    langDictionary: null,
    acceptedLabel: "",
    lblTotalBill: "",
    clmCount: "",
    clmTotal: "",
    clmValue: "",
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
    hideBackButton: false,
    showAnim: false,
}

const mutations = {
    [CashAcceptingMutations.SET_DATA](state, payload) {
        state.flagDetail = false
        state.totalBill = payload.totalBill
        state.accepted = payload.accepted
        state.customer = payload.customer
        state.hideBackButton = true
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Load Data Mutation`, {payload: payload, state: state})
    },
    [CashAcceptingMutations.CHANGE_DATA](state, payload) {
        state.flagDetail = payload.flagShowCashDetail
        state.accepted = payload.accepted;
        state.showAnim = true
        state.hideBackButton = true
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Data Mutation`, {payload: payload, state: state})
    },
    [CashAcceptingMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload
    },
    [CashAcceptingMutations.SET_BUTTON_LABELS](state) {
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
        state.title = state.langDictionary["cashAcceptModul.header"];
        state.infoMessage = state.langDictionary["cashAcceptModul.lblInfo"];
        state.acceptedLabel = state.langDictionary["cashAcceptModul.lblAccepted"];
        state.lblTotalBill = state.langDictionary["cashAcceptModul.lblTotalBill"]
        state.clmCount = state.langDictionary["cashAcceptModul.clmCount"]
        state.clmTotal = state.langDictionary["cashAcceptModul.clmTotal"]
        state.clmValue = state.langDictionary["cashAcceptModul.clmValue"]
    },
    [CashAcceptingMutations.SET_CASH_MODELS](state, payload) {
        state.cashModels = [];
        var banknoteModels = [];
        var coinModels = [];

        if (payload.acceptedBanknotes != null) {
            banknoteModels = [...payload.acceptedBanknotes].map((banknote) => {
                return {
                    count: banknote.count,
                    total: banknote.total,
                    type: banknote.type.value,
                };
            });
        }
        if (payload.acceptedCoins != null) {
            coinModels = [...payload.acceptedCoins].map((coin) => {
                return {
                    count: coin.count,
                    total: coin.total,
                    type: coin.type.value,
                };
            });
        }
        state.cashModels = [...banknoteModels, ...coinModels];
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Set Cash Models`, {payload: payload, cashModels: state.cashModels})
    },
    [CashAcceptingMutations.REFRESH](state) {
        state.componentKey++
    }
}

const actions = {
    loadData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Load Data Action`, payload)
        if (payload != null) {
            commit(CashAcceptingMutations.SET_DATA, payload.data)
            commit(CashAcceptingMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(CashAcceptingMutations.SET_BUTTON_LABELS)
        }
    },
    changeData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Data Action`, payload)
        if (payload != null) {
            commit(CashAcceptingMutations.CHANGE_DATA, payload.data);
            commit(CashAcceptingMutations.SET_CASH_MODELS, payload.data)
            commit(CashAcceptingMutations.REFRESH)

        }
        
        //dispatch("changeReceived")

        // var body = {};
        // body.cmd = CashAcceptingCommand.ChangeReceived;
        // body.screen = ScreenEnum.CashAccepting;
        // body.data = "";
        // console.log("Change Received: ", body);
        // Vue.prototype.$invokeMethod(body);
    },
    changeReceived() {
        var body = {};
        body.cmd = CashAcceptingCommand.ChangeReceived;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log("Change Received: ", body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Received`, body)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Back`, body)
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Cancel`, body)
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}