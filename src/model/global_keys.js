import { ScreenEnum } from "../enums/screen_enum";
import { RoutePaths } from "../router/route-paths";

export class GlobalKeys {
    static clientKey = "";
    static current_path = RoutePaths.Home;
    static current_screen = ScreenEnum.Init;

    static setClientKey(key) {
        this.clientKey = key;
    }

    static setCurrentScreen(screen) {
        this.current_screen = screen;
    }

    static setCurrentPath(path){
        this.current_path = path;
    }
}