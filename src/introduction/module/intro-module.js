import Vue from "vue"
import { ScreenEnum } from '../../enums/screen_enum'
// import { Body } from "../../plugins/model/scr_packet"
import { InitCommand } from "../../enums/command"
import { IntroMutations } from "../../enums/mutations.js"

const state = {
    type: null,
    slider: null,
    imageLength: 0,
    video: null,
}

const mutations = {
    [IntroMutations.SET_TYPE](state, payload) {
        state.type = payload;
    },
    [IntroMutations.SET_SLIDER](state, payload) {
        state.slider = payload;
        state.imageLength = payload.slideList.length;
    },
    [IntroMutations.SET_VIDEO](state, payload) {
        state.video = payload;
    },
}

const actions = {
    loadData({commit}, payload){
        if(payload != null) {
            commit(IntroMutations.SET_TYPE, payload.data.type)
            commit(IntroMutations.SET_SLIDER, payload.data.slider)
            commit(IntroMutations.SET_VIDEO, payload.data.video)
        }
    },
    nextScreen() {
        var body = {};
        body.cmd = InitCommand.InitStartTransaction;
        body.screen = ScreenEnum.Init;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}