import Vue from "vue";
import { CommonCommand, DebtSelectionCommand } from "../../enums/command";
import { DebtSelectionMutations } from "../../enums/mutations";
import { ScreenEnum } from '../../enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = "debtSelectionModule";

export const DebtSelectionActionTypes = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    SELECTED: `${moduleName}/itemSelected`,
    ALL_SELECTED: `${moduleName}/allItemSelected`,
    SELECTED_BILL_GROUP: `${moduleName}/selectedBillGroup`,
    SELECTED_PROC: `${moduleName}/selectedProcModel`,
    CHANGE_PANEL_REF: `${moduleName}/changePanelRef`,
    CHANGE_DEBT_PAGE: `${moduleName}/changeDebtPage`,
}

const state = {
    title: "",
    totalPayment: 0,
    totalBillCount: 0,
    procList: [],
    billList: [],

    selectedProcModel: null,
    selectedGroup: null,
    selectedAllGroupBills: false,

    langDictionary: null,
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
    lblTotalAmount: "",
    lblSelectedBillCount: "",
    lblTotalPayment: "",

    clmYear: "",
    clmTotal: "",
    clmSelect: "",
    clmPeriod: "",
    clmLastPaymentDate: "",
    clmInterest: "",
    clmDescription: "",
    clmAmount: "",
    clmBillNumber: "",

    hideBackButton: false,
}

const mutations = {
    [DebtSelectionMutations.SET_DEBTS](state, payload) {
        state.hideBackButton = false
        state.procList = payload.procs.map(proc => {
            var index = payload.summary.procs.findIndex(e => e.procId == proc.procId)
            var summary = payload.summary.procs[index]
            proc.totalPayment = summary.procTotalAmount
            proc.totalBillCount = summary.procTotalBillCount

            if (proc.selected) {
                proc.billGroup.forEach(group => {
                    if (group.selected) {
                        state.billList = group.bills
                        state.selectedGroup = group
                    }
                });
            }

            return proc
        })
    },
    [DebtSelectionMutations.CHANGE_SELECTED_BILLS](state, payload) {
        console.log("ChangeData'ya girdi : ", payload)
        state.hideBackButton = true
        state.totalPayment = payload.summary.totalAmount
        state.totalBillCount = payload.summary.totalBillCount
        state.billList = []
        state.procList = state.procList.map(proc => {
            var index = payload.summary.procs.findIndex(e => e.procId == proc.procId)
            var summary = payload.summary.procs[index]
            proc.totalPayment = summary.procTotalAmount
            proc.totalBillCount = summary.procTotalBillCount

            var procData = payload.selected[proc.procId.toString()]
            if (procData != undefined) {
                proc.billGroup = proc.billGroup.map(group => {
                    var groupData = procData[group.billGroupId.toString()]
                    if (groupData != undefined) {
                        if (state.selectedGroup.billGroupId == group.billGroupId && proc.procId == payload.selectedProcId) {
                            console.log("group : ", group)
                            console.log("selected Group : ", state.selectedGroup)
                            state.selectedAllGroupBills = groupData.flagAll
                        }
                        group.flagAll = groupData.flagAll
                        group.bills = group.bills.map(bill => {
                            var exist = groupData.selected.includes(bill.billId)
                            bill.selected = exist;
                            return bill;
                        })
                    }
                    else {
                        group.flagAll = false
                        group.bills = group.bills.map(bill => {
                            bill.selected = false
                            return bill
                        })
                    }


                    return group

                })
            } else
                proc.billGroup = proc.billGroup.map(group => group.bills.map(bill => bill.selected = false))

            if (proc.selected) {
                proc.billGroup.forEach(group => {
                    if (group.selected) state.billList = group.bills
                });
            }

            console.log("proc : ", proc)

            return proc
        })

        state.procList = payload.procs.map(proc => {
            var index = payload.summary.procs.findIndex(e => e.procId == proc.procId)
            var summary = payload.summary.procs[index]
            proc.totalPayment = summary.procTotalAmount
            proc.totalBillCount = summary.procTotalBillCount

            if (proc.selected) {
                proc.billGroup.forEach(group => {
                    if (group.selected) state.billList = group.bills
                });
            }

            return proc
        })
    },
    [DebtSelectionMutations.SET_SELECTED_PROC](state, payload) {
        state.billList = []
        state.procList = state.procList.map(proc => {
            proc.selected = false
            if (proc.procId == payload.procId) {
                proc.selected = true
                proc.billGroup.forEach(group => {
                    if (group.selected) {
                        state.billList = group.bills
                        state.selectedGroup = group
                        state.selectedAllGroupBills = group.flagAll
                    }
                });
            }
            return proc
        })
    },
    [DebtSelectionMutations.SET_SELECTED_BILL_GROUP](state, payload) {
        console.log("selected bill group : ", payload)
        state.selectedGroup = payload.billGroup
        state.selectedAllGroupBills = payload.billGroup.flagAll
        state.billList = []
        var index = state.procList.findIndex(proc => proc.procId == payload.procId)
        state.procList[index].billGroup = state.procList[index].billGroup.map(group => {
            group.selected = false
            if (group.billGroupId == payload.billGroup.billGroupId) {
                group.selected = true
                state.billList = group.bills
            }
            return group
        })
    },
    [DebtSelectionMutations.SET_TOTAL_PAYMENT](state, payload) {
        state.totalPayment = payload.totalAmount;
    },
    [DebtSelectionMutations.SET_TOTAL_BILL_COUNT](state, payload) {
        state.totalBillCount = payload.totalBillCount;
    },
    [DebtSelectionMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload;
    },
    [DebtSelectionMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["debtSelModul.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
        state.lblTotalAmount = state.langDictionary[
            "debtSelModul.lblTotalAmount"
        ];
        state.lblSelectedBillCount = state.langDictionary[
            "debtSelModul.lblSelectedBillCount"
        ];
        state.lblTotalPayment = state.langDictionary[
            "debtSelModul.lblTotalPayment"
        ];
        state.clmYear = state.langDictionary["debtSelModul.clmYear"];
        state.clmTotal = state.langDictionary["debtSelModul.clmTotal"];
        state.clmSelect = state.langDictionary["debtSelModul.clmSelect"];
        state.clmPeriod = state.langDictionary["debtSelModul.clmPeriod"];
        state.clmLastPaymentDate = state.langDictionary[
            "debtSelModul.clmLastPaymentDate"
        ];
        state.clmInterest = state.langDictionary[
            "debtSelModul.clmInterest"
        ];
        state.clmDescription = state.langDictionary[
            "debtSelModul.clmDescription"
        ];
        state.clmAmount = state.langDictionary["debtSelModul.clmAmount"];
        state.clmBillNumber = state.langDictionary[
            "debtSelModul.clmBillNumber"
        ];
    },
}

const actions = {
    loadData({ commit }, payload) {
        if (payload != null) {
            commit(DebtSelectionMutations.SET_DEBTS, payload.data)
            commit(DebtSelectionMutations.SET_TOTAL_PAYMENT, payload.data.summary)
            commit(DebtSelectionMutations.SET_TOTAL_BILL_COUNT, payload.data.summary)
            commit(DebtSelectionMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(DebtSelectionMutations.SET_BUTTON_LABELS)
        }
    },
    changeData({ commit }, payload) {
        if (payload != null) {
            commit(DebtSelectionMutations.CHANGE_SELECTED_BILLS, payload.data)
        }
    },
    selectedBillGroup({ commit }, payload) {
        commit(DebtSelectionMutations.SET_SELECTED_BILL_GROUP, payload)
    },
    selectedProcModel({ commit }, payload) {
        commit(DebtSelectionMutations.SET_SELECTED_PROC, payload)
    },
    changePanelRef({ commit }, payload) {
        commit(DebtSelectionMutations.SET_PANEL_REF, payload)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.DebtSelection;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.DebtSelection;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ commit }) {
        var body = {};
        body.cmd = DebtSelectionCommand.Completed;
        body.screen = ScreenEnum.DebtSelection;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] NEXT`, body)
        Vue.prototype.$invokeMethod(body);
    },
    changeDebtPage({ commit }, payload) {
        commit(DebtSelectionMutations.SET_DEBT_PAGE, payload)
    },
    // eslint-disable-next-line no-unused-vars
    itemSelected({ commit, state, dispatch }, payload) {
        var inChangedDebtSelData = {
            procId: payload.proc.procId,
            billGroupId: state.selectedGroup.billGroupId,
            billId: payload.bill.billId,
            flagCheckAll: false,
        };

        var body = {};
        body.cmd = DebtSelectionCommand.Selected;
        body.screen = ScreenEnum.DebtSelection;
        body.data = JSON.stringify(inChangedDebtSelData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    allItemSelected({ commit, state, dispatch }, payload) {
        var inChangedDebtSelData = {
            procId: payload.proc.procId,
            billGroupId: state.selectedGroup.billGroupId,
            billId: 0,
            flagCheckAll: !state.selectedAllGroupBills,
        };

        var body = {};
        body.cmd = DebtSelectionCommand.Selected;
        body.screen = ScreenEnum.DebtSelection;
        body.data = JSON.stringify(inChangedDebtSelData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}