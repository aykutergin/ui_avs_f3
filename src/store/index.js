import Vue from 'vue'
import Vuex from 'vuex'
import appModule from '../app/module/app-module'
import calculateCreditModule from '../calculate-credit/module/calculate-credit-module'
import cardReaderModule from '../card-reader/module/card-reader-module'
import cashAcceptingModule from '../cash-accepting/module/cash-accepting-module'
import cashTypeModule from '../cash-type-selection/module/cash-type-module'
import creditCardModule from '../credit-card/module/credit-card-module'
import debtSelectionModule from '../debt-list/module/debt-selection-module'
import inquiryModule from '../debt_inquiry/module/debt-inquiry-module'
import eksLoadModule from '../eks-load/module/eks-load-module'
import { ScreenType } from '../enums/screen_type'
import initializeModule from '../initialize/module/initialize_module'
import introModule from '../introduction/module/intro-module'
import langModule from '../language/module/lang-module'
import paybackModule from '../payback/module/payback-module'
import printModule from '../print/module/print-module'
import selectionModule from '../process-service/module/selection-module'
import messageModule from '../_core/components/message-wrapper/module/message_module'
import loginModule from '../__operator_screens/login/module/login_module'

Vue.use(Vuex)

export const SET_CONNECTION = "setBody";
export const SET_LOADING = "setLoading";
const SET_SCREEN_TYPE = "setScreenType";

export default new Vuex.Store({
  state: {
    screenType: ScreenType.Landscape,
  },
  mutations: {
    [SET_SCREEN_TYPE](state, payload) {
      state.screenType = payload;
    }
  },
  actions: {
    getScreenType({ commit }, payload) {
      if (payload > 1.0) commit(SET_SCREEN_TYPE, ScreenType.Landscape);
      else commit(SET_SCREEN_TYPE, ScreenType.Portrait);
    }
  },
  modules: {
    appModule,
    messageModule,
    introModule,
    langModule,
    selectionModule,
    inquiryModule,
    initializeModule,
    debtSelectionModule,
    cashTypeModule,
    cashAcceptingModule,
    creditCardModule,
    paybackModule,
    printModule,
    cardReaderModule,
    calculateCreditModule,
    loginModule,
    eksLoadModule,
  }
})
