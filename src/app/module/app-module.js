import Vue from 'vue';
import { CalculateCreditCommand, CardReaderCommand, CashAcceptingCommand, CashTypeCommand, CreditCardCommand, DebtQueryCommand, DebtSelectionCommand, EksLoadCommand, InitializeCommand, LanguageCommand, MessageCommand, PaybackCommand, PrintCommand, UnitCommand } from '../../enums/command';
import { ScreenEnum } from '../../enums/screen_enum';
import { Module } from '../../helpers/module';
import { GlobalKeys } from '../../model/global_keys';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';
import router from '../../router';
import { RoutePaths } from '../../router/route-paths';

const that = Vue.prototype;

const state = {
}

const mutations = {}

const actions = {
    connectAppMessage({ dispatch }) {
        that.$hub.on("AppMessage", (jsonData) => {
            console.log(jsonData);
            var scrModel = JSON.parse(jsonData);
            console.log(scrModel);

            var screen = scrModel.body.screen;

            //#region Data Parsing
            var payload = {}
            payload.data = (scrModel.body.data != null) ? JSON.parse(scrModel.body.data) : null;
            payload.langDictionary = (scrModel.body.languageValueData != null) ? JSON.parse(scrModel.body.languageValueData) : null;
            console.log(payload)
            //#endregion

            //#region GlobalKeys Settings
            GlobalKeys.setClientKey(scrModel.header.client);
            //if (payload.data != null) GlobalKeys.setCurrentScreen(screen); # Merve'nin İşi
            
            LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[APP MODULE] CURRENT SCREEN`, screen)
            LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[APP MODULE] PAYLOAD`, payload)
            GlobalKeys.setCurrentScreen(screen);
            //#endregion

            //#region Message Management
            var messageData = (scrModel.body.messageData != null) ? JSON.parse(scrModel.body.messageData) : null;
            if (messageData != null) {
                if (scrModel.body.cmd == MessageCommand.Show) {
                    dispatch("messageModule/setLangDictionary", payload.langDictionary)
                    dispatch("messageModule/showMessage", messageData);
                } else if (scrModel.body.cmd == MessageCommand.Hide) {
                    dispatch("messageModule/hideMessage", messageData);
                }
            }

            if (GlobalKeys.current_screen != payload.data.screen) {
                dispatch("messageModule/clearAll");
            }
            //#endregion


            //#region Screen Management

            switch (screen) {
                case ScreenEnum.Initialize:
                    console.log("İnitialize");
                    if (scrModel.body.cmd == InitializeCommand.InitializeLoad && GlobalKeys.current_path != RoutePaths.Initialize) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Initialize);
                        router.replace({ name: RoutePaths.Initialize });
                    } else if (scrModel.body.cmd == InitializeCommand.Change) dispatch(Module.setModuleChange(screen), payload);
                    break;
                case ScreenEnum.Init:
                    console.log("İnit");
                    if (GlobalKeys.current_path != RoutePaths.Introduction) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Introduction);
                        router.replace({ name: RoutePaths.Introduction });
                    }
                    break;
                case ScreenEnum.Language:
                    console.log("Language")
                    if (scrModel.body.cmd == LanguageCommand.LanguageLoad && GlobalKeys.current_path != RoutePaths.Language) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Language);
                        router.replace({ name: RoutePaths.Language });
                    }
                    break;
                case ScreenEnum.Unit:
                    console.log("Unit")
                    if (scrModel.body.cmd == UnitCommand.UnitLoad && GlobalKeys.current_path != RoutePaths.ProcService) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.ProcService);
                        router.replace({ name: RoutePaths.ProcService });
                    } else if (scrModel.body.cmd == UnitCommand.Changed) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.DebtQuery:
                    console.log("DebtQuery")
                    if (scrModel.body.cmd == DebtQueryCommand.DebtQueryLoad && GlobalKeys.current_path != RoutePaths.DebtInquiry) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.DebtInquiry);
                        router.replace({ name: RoutePaths.DebtInquiry });
                    }
                    break;
                case ScreenEnum.DebtSelection:
                    console.log("DebtSelection");
                    if (scrModel.body.cmd == DebtSelectionCommand.Load && GlobalKeys.current_path != RoutePaths.DebtSelection) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.DebtSelection);
                        router.replace({ name: RoutePaths.DebtSelection });
                    } else if (scrModel.body.cmd == DebtSelectionCommand.Changed) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.CashType:
                    console.log("CashType")
                    if (scrModel.body.cmd == CashTypeCommand.CashTypeLoad && GlobalKeys.current_path != RoutePaths.CashType) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CashType);
                        router.replace({ name: RoutePaths.CashType });
                    }
                    break;
                case ScreenEnum.CashAccepting:
                    console.log("CashAccepting")
                    if (scrModel.body.cmd == CashAcceptingCommand.CashAcceptingLoad && GlobalKeys.current_path != RoutePaths.CashAccepting) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CashAccepting);
                        router.replace({ name: RoutePaths.CashAccepting });
                    } else if (scrModel.body.cmd == CashAcceptingCommand.CashAcceptingUpdate) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.Payback:
                    console.log("Payback")
                    if (scrModel.body.cmd == PaybackCommand.PaybackLoad && GlobalKeys.current_path != RoutePaths.Payback) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Payback);
                        router.replace({ name: RoutePaths.Payback });
                    }
                    break;
                case ScreenEnum.CreditCard:
                    console.log("CreditCard")
                    if (scrModel.body.cmd == CreditCardCommand.CreditCardLoad && GlobalKeys.current_path != RoutePaths.CreditCard) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CreditCard);
                        router.replace({ name: RoutePaths.CreditCard });
                    } else if (scrModel.body.cmd == CreditCardCommand.CreditCardChange) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.CardReader:
                    console.log("CardReader")
                    if (scrModel.body.cmd == CardReaderCommand.CardReaderLoad && GlobalKeys.current_path != RoutePaths.CardReader) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CardReader);
                        router.replace({ name: RoutePaths.CardReader });
                    }
                    break;
                case ScreenEnum.CalculateCredit:
                    console.log("CalculateCredit")
                    if (scrModel.body.cmd == CalculateCreditCommand.CalculateCreditLoad && GlobalKeys.current_path != RoutePaths.CalculateCredit) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CalculateCredit);
                        router.replace({ name: RoutePaths.CalculateCredit });
                    }
                    break;
                case ScreenEnum.Print:
                    console.log("Print")
                    if (scrModel.body.cmd == PrintCommand.PrintLoad && GlobalKeys.current_path != RoutePaths.Print) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Print);
                        router.replace({ name: RoutePaths.Print });
                    }
                    break;
                case ScreenEnum.EksLoad:
                    console.log("EksLoad")
                    console.log("Eks body cmd : ",scrModel.body.cmd)
                    if (scrModel.body.cmd == EksLoadCommand.Load && GlobalKeys.current_path != RoutePaths.EksLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.EksLoad);
                        router.replace({ name: RoutePaths.EksLoad });
                    } else if (scrModel.body.cmd == EksLoadCommand.Change) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
            }
            //#endregion
        });
    }
}

export default {
    state,
    mutations,
    actions,
}