import Vue from "vue";
import { PaybackCommand } from "../../enums/command";
import { PaybackMutations } from "../../enums/mutations";
import { ScreenEnum } from "../../enums/screen_enum";
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = 'paybackModule'

export const PaybackActions = {
    timeout: `${moduleName}/sendTimeout`
}

const state = {
    componentKey: 1,
    message: "",
    timeout: 0,
}

const mutations = {
    [PaybackMutations.SET_MESSAGE](state, payload) {
        state.message = payload.infoMessage;
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Set Message Mutation`, {payload: payload, message: state.message})
    },
    [PaybackMutations.SET_TIMEOUT](state, payload) {
        state.timeout = payload.timeout;
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Set Timeout Mutation`, {payload: payload, timeout: state.timeout})
    }
}

const actions = {
    loadData({commit}, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Load Data`, payload)
        commit(PaybackMutations.SET_MESSAGE, payload.data)
        commit(PaybackMutations.SET_TIMEOUT, payload.data)
    },
    // changeData({commit}, payload) {
    //     LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Load Data`, payload)
    //     commit(PaybackMutations.SET_MESSAGE, payload.data)
    // },
    sendTimeout() {
        var body = {};
        body.cmd = PaybackCommand.Timeout;
        body.screen = ScreenEnum.Payback;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Time is up`, body)
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}