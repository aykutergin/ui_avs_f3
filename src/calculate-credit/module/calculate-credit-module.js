import Vue from "vue";
import { CalculateCreditCommand, CommonCommand } from "../../enums/command";
import { CalculateCreditMutations } from "../../enums/mutations";
import { ScreenEnum } from '../../enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = "calculateCreditModule";

export const CalculateCreditActionTypes = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CHANGE_AMOUNT: `${moduleName}/changeAmount`,
}

const state = {
    title: "",
    langDictionary: null,
    subscriber: null,
    enteredAmount: null,
    showKeyboard: false,
    showNumberpad: false,
    lblMinAmount: "",
    lblMaxAmount: "",
    lblAmount: null,
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
    lblTon: "",
    lblInfo: "",
    lblCardNo: "",
    lblSubscriberNo: "",
    lblMain: "",
    lblReserved: "",
}

const mutations = {
    [CalculateCreditMutations.SET_DATA](state, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Set Data Mutation Started`)
        // state.title = `${state.lblMinAmount} ${state.subscriber.minAmount} - ${state.lblMaxAmount} ${state.subscriber.maxAmount} - ${state.lblInfo}`
        state.enteredAmount = null
        state.showNumberpad = true
        state.showKeyboard = false
        state.subscriber = payload
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Set Data Mutation`, { payload: payload, state: state })
    },
    [CalculateCreditMutations.CHANGE_AMOUNT](state, payload) {
        if (state.enteredAmount == null) state.enteredAmount = ""
        if (payload == null) {
            state.enteredAmount = state.enteredAmount.slice(0, -1)
        } else
            state.enteredAmount += payload.toString();

        console.log(state.enteredAmount)
    },
    [CalculateCreditMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload;
    },
    [CalculateCreditMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["calculateCreditModul.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
        state.lblMinAmount = state.langDictionary["calcCreditModul.lblMin"]
        state.lblMaxAmount = state.langDictionary["calcCreditModul.lblMax"]
        state.lblAmount = state.langDictionary["calcCreditModul.lblAmount"]
        state.lblTon = state.langDictionary["calcCreditModul.lblTon"]
        state.lblInfo = state.langDictionary["calcCreditModul.lblInfo"]
        state.lblCardNo = state.langDictionary["calcCreditModul.lblCardNo"]
        state.lblSubscriberNo = state.langDictionary["calcCreditModul.lblSubscriberNo"]
        state.lblMain = state.langDictionary["calcCreditModul.lblMain"]
        state.lblReserved = state.langDictionary["calcCreditModul.lblReserved"]
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("calculateCreditModule loadData")
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Load Data Action`, payload)
        if (payload != null) {
            commit(CalculateCreditMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(CalculateCreditMutations.SET_BUTTON_LABELS)
            commit(CalculateCreditMutations.SET_DATA, payload.data.eksSubscriber)
        }
    },
    changeAmount({ commit }, payload) {
        commit(CalculateCreditMutations.CHANGE_AMOUNT, payload)
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.CalculateCredit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ state }) {
        var amount = state.enteredAmount

        if(amount == null) amount = 0;

        var model = {};
        model.amount = amount

        console.log(model);
        var body = {};
        body.cmd = CalculateCreditCommand.CalculateCredit;
        body.screen = ScreenEnum.CalculateCredit;
        body.data = JSON.stringify(model);

        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L1, `[CALCULATE CREDIT] Next Action : `, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}