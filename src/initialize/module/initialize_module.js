
import { InitializeMutations } from '../../enums/mutations.js';

const moduleName = "initializeModule";

export const InitializeActionTypes = {
    UPDATED: `${moduleName}/updated`,
    LOAD: `${moduleName}/load`,
    CANCEL: `${moduleName}/completed`,
}

const state = {
    messages: [],
}

const mutations = {
    [InitializeMutations.SET_MESSAGES](state, payload) {
        console.log(payload);
        state.messages = [];
        payload.forEach(element => {
            state.messages.push(element)
        });
    }
}

const actions = {
    loadData({ commit }, payload) {
        console.log("loadData çalıştı");
        console.log(payload.messages);
        if (payload != null) commit(InitializeMutations.SET_MESSAGES, payload.data.messages);
    },
    changeData({ commit }, payload) {
        console.log("changeData çalıştı");
        console.log(payload.messages);
        if (payload != null) commit(InitializeMutations.SET_MESSAGES, payload.data.messages);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}