import Vue from "vue";
import { CashTypeCommand, CommonCommand } from "../../enums/command";
import { CashTypeMutations } from "../../enums/mutations";
import { ScreenEnum } from '../../enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../plugins/log_exports';

const moduleName = "cashTypeModule";

export const CashTypeActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CASH_TYPE_SELECTED: `${moduleName}/cashTypeSelected`,
}

const state = {
    componentKey: 1,
    payTypes: [],
    langDictionary: null,
    backButtonVisible: true,
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
}

const mutations = {
    [CashTypeMutations.SET_PAY_TYPES](state, payload) {
        state.backButtonVisible = payload.backButtonVisible
        state.payTypes = payload.payTypes;
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] Set Data Mutation`, { payload: payload, state: state })
    },
    [CashTypeMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload
    },
    [CashTypeMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["payTypeModul.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    },
}

const actions = {
    loadData({commit}, payload){
        if(payload != null) {
            LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] Load Data`, payload)
            commit(CashTypeMutations.SET_PAY_TYPES, payload.data)
            commit(CashTypeMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(CashTypeMutations.SET_BUTTON_LABELS)
        }
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashType;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.CashType;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    cashTypeSelected({commit}, selectedType){
        var inSelectedPayTypeData = {};
        inSelectedPayTypeData.payMethodId = selectedType.payMethodId;

        var body = {};
        body.cmd = CashTypeCommand.CashTypeSelected;
        body.screen = ScreenEnum.CashType;
        body.data = JSON.stringify(inSelectedPayTypeData);
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] SELECTED`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}