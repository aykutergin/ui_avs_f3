import Vue from "vue";
import { MessageCommand } from "../../../../enums/command";
import { MessageMutations } from "../../../../enums/mutations.js";
import { GlobalKeys } from "../../../../model/global_keys";
import { LogLevelEnum, LogPriorityEnum, LogService } from '../../../../plugins/log_exports';

export const moduleName = "messageModule";

const state = {
    messages: [],
    langDictionary: null,
    yesLabel: "",
    noLabel: "",
    okLabel: "",
}

const mutations = {
    [MessageMutations.ADD_MESSAGE](state, payload) {
        if(payload.flagHidePrevMsgs) state.messages = [];
        state.messages.push(payload.message);
    },
    [MessageMutations.DELETE_MESSAGE](state, payload) {
        var value = state.messages.find(x => x.msgId === payload.msgId);
        state.messages = state.messages.filter(item => item !== value);
    },
    [MessageMutations.CLEAR_MESSAGES](state) {
        state.messages = [];
    },
    [MessageMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload
    },
    [MessageMutations.SET_BUTTON_LABELS](state) {
        state.yesLabel = state.langDictionary["global.yes"];
        state.noLabel = state.langDictionary["global.no"];
        state.okLabel = state.langDictionary["global.ok"];
    }
}

const actions = {
    // eslint-disable-next-line no-unused-vars
    responseMessage({commit}, payload){
        var responseData = {
            msgResponse: {
                "msgId": payload.msgId,
                "msgCode": payload.msgCode,
                "response": payload.responseState,
            }
        };
        
        var body = {};
        body.cmd = MessageCommand.Responsed;
        body.screen = GlobalKeys.current_screen;
        body.data = JSON.stringify(responseData);
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[MESSAGE] Response Message`, body)
        Vue.prototype.$invokeMethod(body);
    },
    showMessage({ commit }, payload) {
        console.log(payload);
        if (payload != null) {
            commit(MessageMutations.ADD_MESSAGE, payload);
        }
    },
    hideMessage({ commit }, payload) {
        if (payload != null) {
            commit(MessageMutations.DELETE_MESSAGE, payload);
        }
    },
    clearAll({ commit }) {
        commit(MessageMutations.CLEAR_MESSAGES);
    },
    setLangDictionary({commit}, payload) {
        if(payload != null) {
            commit(MessageMutations.SET_LANG_DICTIONARY, payload)
            commit(MessageMutations.SET_BUTTON_LABELS)
        }
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}