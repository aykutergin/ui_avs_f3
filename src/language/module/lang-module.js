import Vue from "vue"
//import { ScreenEnum } from '../../model/screen_enum'
import { ScreenEnum } from "../../enums/screen_enum"
import { CommonCommand, LanguageCommand } from "../../enums/command"
import { LangMutations } from "../../enums/mutations.js";

const moduleName = "langModule";

export const LanguageActionTypes = {
    SELECTED_LANGUAGE: `${moduleName}/selectedLanguage`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    title: "Language Screen",
    nextLabel: "",
    prevLabel: "",
    cancelLabel: "",
    langs: [],
    langDictionary: null,
}

const mutations = {
    [LangMutations.SET_LANGUAGES](state, payload) {
        state.langs = payload;
    },
    [LangMutations.SET_LANG_DICTIONARY](state, payload) {
        state.langDictionary = payload;
    },
    [LangMutations.SET_BUTTON_LABELS](state) {
        state.title = state.langDictionary["langModul.header"];
        state.nextLabel = state.langDictionary["global.btnNext"];
        state.prevLabel = state.langDictionary["global.btnBack"];
        state.cancelLabel = state.langDictionary["global.btnCancel"];
    }
}

const actions = {
    async loadData({ commit }, payload) {
        if (payload != null) {
            commit(LangMutations.SET_LANGUAGES, payload.data.langs)
            commit(LangMutations.SET_LANG_DICTIONARY, payload.langDictionary)
            commit(LangMutations.SET_BUTTON_LABELS)
        }
        
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.Selection;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = CommonCommand.Cancel;
        body.screen = ScreenEnum.Selection;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
    },

    // eslint-disable-next-line no-unused-vars
    selectedLanguage({ commit }, selectedLanguage) {
        var selectedLanguageModel = {};
        selectedLanguageModel.langId = selectedLanguage.langId;

        var body = {};
        body.cmd = LanguageCommand.LanguageSelected;
        body.screen = ScreenEnum.Language;
        body.data = JSON.stringify(selectedLanguageModel);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}