import LottieAnimation from "lottie-vuejs/src/LottieAnimation.vue";
import Vue from 'vue';
import App from './app/view/App.vue';
import './helpers/extensions';
import LogSignalRPlugin from './plugins/log_signalr_plugin';
import SignalRPlugin from './plugins/signalr_plugin';
import vuetify from './plugins/vuetify';
import router from './router';
import store from './store';

Vue.use(LottieAnimation);

Vue.config.productionTip = false

Vue.use(SignalRPlugin);
Vue.use(LogSignalRPlugin);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
